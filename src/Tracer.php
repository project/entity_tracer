<?php

declare(strict_types=1);

namespace Drupal\entity_tracer;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;

/**
 * The core service for entity tracing.
 */
class Tracer {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Constructs a Tracer object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routeProvider
   *   The route provider.
   */
  public function __construct(
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeManager $entityTypeManager,
    ConfigFactory $configFactory,
    CacheBackendInterface $cache,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    RouteProviderInterface $routeProvider,
  ) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->cache = $cache;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->routeProvider = $routeProvider;
  }

  /**
   * Get and cache the complete reference chain for all enabled entity types.
   *
   * @return array
   *   An array of reference chains.
   */
  public function referenceChainComplete() {
    if (!$this->cache->get('entity_tracer_chain_complete')) {
      $config = $this->configFactory->get('entity_tracer.settings');
      $set_entity_types = $config->get('enabled_entity_types');
      $max_depth = $config->get('max_depth');
      $reference_chains = [];
      $cache_tags = [];
      foreach ($set_entity_types as $entity_type => $entity_label) {
        // Ensure that the cache is invalidated when the chosen entity types
        // are changed.
        $cache_tags[] = 'config:' . $this->entityTypeManager->getDefinition($entity_type)->get('bundle_entity_type') . '_list';
        // Make a separate reference chain for each bundle.
        $bundles = $this->getBundles($entity_type);
        foreach ($bundles as $bundle_key => $bundle) {
          if ($this->getReferenceFields($entity_type, $bundle_key)) {
            $reference_chains[$entity_type . '__' . $bundle_key] = $this->getRawReferenceChain($entity_type, $bundle_key, $max_depth);
          }
        }
      }

      // Make sure that cache also gets invalidated when the config is changed.
      $cache_tags[] = 'config:entity_tracer.settings';
      $cache_tags[] = 'entity_field_info';
      $this->cache->set('entity_tracer_chain_complete', $reference_chains, Cache::PERMANENT, $cache_tags);
    }
    else {
      $reference_chains = $this->cache->get('entity_tracer_chain_complete')->data;
    }

    return $reference_chains;
  }

  /**
   * Get all reference fields for a given entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   An array of reference fields.
   */
  public function getReferenceFields($entity_type, $bundle) {
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $reference_fields = [];
    foreach ($fields as $field_name => $field_definition) {
      if ($field_definition instanceof FieldConfig) {
        $type = $field_definition->getType();
        if ($type === 'entity_reference' || $type === 'entity_reference_revisions') {
          $reference_fields[$field_name] = $field_definition;
        }
      }
    }
    return $reference_fields;
  }

  /**
   * Get the raw reference chain for a given entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param int $max_depth
   *   The maximum depth of the resulting array.
   *
   * @return array
   *   An array of reference chains.
   */
  public function getRawReferenceChain($entity_type, $bundle, $max_depth) {
    $reference_fields = $this->getReferenceFields($entity_type, $bundle);
    $chain = [];
    if ($max_depth === 0) {
      return $chain;
    }
    // Build out the reference chain recursively.
    foreach ($reference_fields as $field_machine_name => $field_definition) {
      $targets = $field_definition->getSetting('handler_settings')['target_bundles'];
      $target_type = $field_definition->getSetting('target_type');
      $i = 0;
      foreach ($targets as $target) {
        $label = $target_type . '__' . $target;
        $field_human_readable_name = $this->getFieldLabel($entity_type, $bundle, $field_machine_name);
        $field_label = $field_machine_name . '__' . $field_human_readable_name;
        // Stop building the chain once it hits the node level. Otherwise, the
        // chain would keep building endlessly.
        $final_entities = ['node'];
        if (in_array($target_type, $final_entities)) {
          // Amend the final value for rendering.
          $i++;
          $chain[$field_label]['end_' . $i] = $label;
        }
        else {
          $reference_fields = $this->getRawReferenceChain($target_type, $target, $max_depth - 1);
          if (!empty($reference_fields)) {
            $chain[$field_label][$label] = $reference_fields;
          }
          else {
            $chain[$field_label][$label] = [];
          }
        }
      }
    }
    return $chain;
  }

  /**
   * Search the reference chain for a given entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $direction
   *   The direction of the search. Can be 'up' or 'down'.
   *
   * @return array
   *   An array of reference chains.
   */
  public function searchReferenceChain($entity_type, $bundle, $direction) {
    $complete_chain = $this->referenceChainComplete();
    $entity_label = $entity_type . '__' . $bundle;
    $return_data = [];
    if ($direction === 'up') {
      $data = $this->recursiveArraySearch($complete_chain, $entity_label);
      foreach ($data as $reference_chain) {
        // Recursive array search sometimes returns an empty path to the entity
        // being searched for.
        if (empty($reference_chain)) {
          continue;
        }
        $label = array_shift($reference_chain);
        $result = $this->buildNestedArray($reference_chain, $entity_label);
        $return_data[$label][] = $result;
      }
    }
    elseif ($direction === 'down') {
      $return_data = $complete_chain[$entity_label];
    }
    return $return_data;
  }

  /**
   * Recursively search an array for a given value.
   *
   * @param array $array
   *   The array to search.
   * @param mixed $search_value
   *   The value to search for.
   * @param array $path
   *   The path to the current value.
   *
   * @return array
   *   An array of matching paths.
   */
  public function recursiveArraySearch($array, $search_value, $path = []) {
    $matching_paths = [];
    foreach ($array as $key => $value) {
      if ($key === $search_value || $value === $search_value) {
        $matching_paths[] = $path;
      }
      if (is_array($value)) {
        $sub_path = array_merge($path, [$key]);
        $matching_paths = array_merge($matching_paths, $this->recursiveArraySearch($value, $search_value, $sub_path));
      }
    }
    return $matching_paths;
  }

  /**
   * Build a nested array from a list of keys.
   *
   * @param array $keys
   *   The keys to build the array from.
   * @param mixed $bundle
   *   The value to set at the end of the array.
   *
   * @return array
   *   An array of nested arrays.
   */
  public function buildNestedArray($keys, $bundle) {
    $result = [];
    $current = &$result;
    $final_key = count($keys) - 1;
    foreach ($keys as $key => $value) {
      if ($key === $final_key) {
        $current[$value] = $bundle;
      }
      else {
        $current[$value] = [];
      }
      $current = &$current[$value];
    }
    return $result;
  }

  /**
   * Create the final label and link for a given entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param bool $link
   *   Whether to return a link or not.
   *
   * @return string
   *   The human readable label and link for the entity.
   */
  public function getBundleLabel($entity_type, $bundle, $link = FALSE) {
    // Retrieve bundle information.
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
    $label = $bundles[$bundle]['label'] . ' (' . $entity_type . ':' . $bundle . ')';
    if (!$link) {
      return $label;
    }

    $route_name = 'entity.' . $entity_type . '.field_ui_fields';
    // Make sure the route exists.
    $route_exists = count($this->routeProvider->getRoutesByNames([$route_name])) === 1;
    if ($route_exists) {
      $route = $this->routeProvider->getRouteByName($route_name);
      $route_parameters = $route->compile()->getVariables();
      // Create the URL parameters based on route parameters and entity type.
      $url_params = [];
      foreach ($route_parameters as $param) {
        if ($param === 'node_type' || $param === 'entity_type') {
          $url_params[$param] = $bundle;
        }
        else {
          $url_params[$param] = $bundle;
        }
      }
      // Return the edit fields page of the entity type and bundle.
      $edit_url = Url::fromRoute($route_name, $url_params);
      return Link::fromTextAndUrl($label, $edit_url)->toString();
    }
    return $label;
  }

  /**
   * Get the human readable label for a given field.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $field_machine_name
   *   The field machine name.
   *
   * @return string
   *   The human readable label for the field.
   */
  public function getFieldLabel($entity_type, $bundle, $field_machine_name) {
    $field = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    return $field[$field_machine_name]->getLabel();
  }

  /**
   * Get all bundles for a given entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return array
   *   An array of bundles [human readable name => machine name].
   */
  public function getBundles($entity_type) {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
    $result = [];
    foreach ($bundles as $name => $bundle_info) {
      $result[$name] = $bundle_info['label'] . ' (' . $name . ')';
    }
    return $result;
  }

}
