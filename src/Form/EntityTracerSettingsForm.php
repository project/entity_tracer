<?php

declare(strict_types=1);

namespace Drupal\entity_tracer\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure entity_tracer settings for this site.
 */
final class EntityTracerSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityTracerSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_tracer_entity_tracer_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['entity_tracer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entities'),
      '#options' => $this->getContentEntities(),
      '#default_value' => $this->config('entity_tracer.settings')->get('enabled_entity_types') ?? [],
      '#description' => $this->t('Select the entities you want to trace.'),
    ];

    $form['max_depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Depth'),
      '#default_value' => $this->config('entity_tracer.settings')->get('max_depth') ?? 10,
      '#description' => $this->t('Set the maximum depth of the entity trace. This is mainly to prevent timeouts if the entity reference chain is too long.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get all content entities.
   *
   * @return array
   *   An array of content entities [machine_name => label].
   */
  public function getContentEntities() {
    $entity_types = $this->entityTypeManager->getDefinitions();

    $result = [];
    foreach ($entity_types as $name => $entity_type) {
      if ($entity_type->getGroupLabel()->render() === 'Content') {
        $result[$name] = is_string($entity_type->get('label')) ? $entity_type->get('label') : $entity_type->get('label')->render();
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $enabled_entity_types = [];
    $disabled_entity_types = [];
    foreach ($form_state->getValue('entity_types') as $key => $value) {
      if ($value) {
        $enabled_entity_types[$key] = $value;
      }
      else {
        $disabled_entity_types[$key] = 0;
      }
    }

    $this->config('entity_tracer.settings')
      ->set('enabled_entity_types', $enabled_entity_types)
      ->set('disabled_entity_types', $disabled_entity_types)
      ->set('max_depth', $form_state->getValue('max_depth'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
