<?php

namespace Drupal\entity_tracer\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_tracer\Tracer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a entity_tracer form.
 */
final class EntityTracer extends FormBase {

  /**
   * The entity tracer.
   *
   * @var \Drupal\entity_tracer\Tracer
   */
  protected $entityTracer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a EntityTracer Form.
   *
   * @param \Drupal\entity_tracer\Tracer $entityTracer
   *   The entity tracer.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(Tracer $entityTracer, ConfigFactory $configFactory, EntityTypeManager $entityTypeManager) {
    $this->entityTracer = $entityTracer;
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_tracer.tracer'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_tracer_entity_tracer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $entity_type_default = array_key_first($this->getSelectedEntityTypes());
    $form['#attached']['library'][] = 'entity_tracer/entity_tracer_form';
    $form['ui'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'entity-tracer-ui'],
    ];
    $form['ui']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h1',
      '#value' => $this->t('Interface'),
    ];

    $entity_type_options = $this->getSelectedEntityTypes();

    if ($entity_type_options) {
      $form['ui']['entity_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Entity Type'),
        '#options' => $this->getSelectedEntityTypes(),
        '#default_value' => $entity_type_default,
        '#ajax' => [
          'callback' => '::createBundleListAjax',
          'wrapper' => 'bundle-list-container',
          'disable-refocus' => TRUE,
        ],
      ];
    }
    else {
      $form['ui']['entity_type'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No entity types enabled. Please enable at least one entity type in the <a href=":tracer_settings">settings</a>.', [
          ':tracer_settings' => Url::fromRoute('entity_tracer.entity_tracer_settings')->toString(),
        ]),
      ];
    }

    if (!empty($form_state->getValue('entity_type'))) {
      $bundle_select_options = $this->entityTracer->getBundles($form_state->getValue('entity_type'));
    }
    else {
      $bundle_select_options = $this->entityTracer->getBundles($entity_type_default);
    }

    // Append a blank entry to the beginning of the array.
    array_unshift($bundle_select_options, 'Select Bundle');

    $form['ui']['bundle_list_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'bundle-list-container'],
    ];

    $form['ui']['bundle_list_container']['bundle_select'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#options' => $bundle_select_options,
      '#submit' => ['::calcTracerSubmit'],
      '#ajax' => [
        'callback' => '::calcTracerAjax',
        'wrapper' => 'entity-tracer-results',
        'disable-refocus' => TRUE,
      ],
    ];

    $form['ui']['direction'] = [
      '#type' => 'radios',
      '#title' => $this->t('Direction'),
      '#default_value' => 'down',
      '#options' => [
        'up' => $this->t('Up'),
        'down' => $this->t('Down'),
      ],
      '#submit' => ['::calcTracerSubmit'],
      '#ajax' => [
        'callback' => '::calcTracerAjax',
        'wrapper' => 'entity-tracer-results',
        'disable-refocus' => TRUE,
      ],
    ];

    $form['entity_tracer_results'] = $this->buildTracerBlock($form_state);

    return $form;
  }

  /**
   * Get the entity types chosen in config.
   */
  public function getSelectedEntityTypes() {
    $config = $this->configFactory->get('entity_tracer.settings');
    $entity_types = $config->get('enabled_entity_types');
    $options = [];

    if (empty($entity_types)) {
      return $options;
    }
    foreach ($entity_types as $entity_type) {
      $options[$entity_type] = $this->entityTypeManager->getDefinition($entity_type)->get('label')->render();
    }
    return $options;
  }

  /**
   * Ajax function for creating the bundle list.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function createBundleListAjax(array &$form, FormStateInterface $form_state) {
    return $form['ui']['bundle_list_container'];
  }

  /**
   * Submit function for calculating the tracer.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function calcTracerSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax function for calculating the tracer.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function calcTracerAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $content = $this->buildTracerBlock($form_state);
    $response->addCommand(new ReplaceCommand('#entity-tracer-results', $content));
    return $response;
  }

  /**
   * Builds the tracer block that can be replaced with ajax.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildTracerBlock($form_state) {
    $container = [
      '#type' => 'container',
      '#attributes' => ['id' => 'entity-tracer-results'],
      'content' => array_merge($this->calculateTracer($form_state), ['#theme' => 'entity_tracer_results']),
    ];

    return $container;
  }

  /**
   * Calculates the tracer array.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function calculateTracer($form_state) {
    $entity_type = $form_state->getValue('entity_type');
    $machine_name = $form_state->getValue('bundle_select');
    $direction = $form_state->getValue('direction');
    if ($entity_type && $machine_name && $direction) {
      $entity_tracer = $this->entityTracer->searchReferenceChain($entity_type, $machine_name, $direction);
      $label = $this->entityTracer->getBundlelabel($entity_type, $machine_name, TRUE);
    }

    return [
      '#results' => $entity_tracer ?? [],
      '#label' => $label ?? '',
      '#direction' => $direction ?? '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
