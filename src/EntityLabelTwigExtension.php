<?php

declare(strict_types=1);

namespace Drupal\entity_tracer;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
final class EntityLabelTwigExtension extends AbstractExtension {


  /**
   * The entity tracer tracer.
   *
   * @var \Drupal\entity_tracer\Tracer
   */
  protected $entityTracer;

  /**
   * Constructs a new EntityLabelTwigExtension object.
   *
   * @param \Drupal\entity_tracer\Tracer $entityTracer
   *   The entity tracer tracer.
   */
  public function __construct(Tracer $entityTracer) {
    $this->entityTracer = $entityTracer;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    $functions[] = new TwigFunction('entity_tracer_label', [$this, 'entityTracerLabel']);
    return $functions;
  }

  /**
   * Returns the entity label as a link to the entity field page.
   *
   * @param string $label
   *   The entity label.
   *
   * @return string
   *   A link in the format human-readable name (entity type:machine name)
   */
  public function entityTracerLabel($label) {
    if (!is_string($label)) {
      return $label;
    }
    $label_values = explode('__', $label, 3);
    if (count($label_values) === 2) {
      $type = $label_values[0];
      $bundle = $label_values[1];
      if (strpos($type, 'field_') === 0) {
        $field_label = $bundle . ' (' . $type . ')';
        $value['#markup'] = '<b>' . $field_label . '</b>';
      }
      else {
        $value = $this->entityTracer->getBundleLabel($type, $bundle, TRUE);
      }
    }

    return $value;
  }

}
