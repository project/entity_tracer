## Entity Tracer

The entity tracer module provides a UI to easily track entity
references across entity types.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

- Go to `/admin/config/development/entity-tracer-settings`.
- Select the entity types you want to track.
- Set the maximum depth (defaults to 10).

Note: This module works by generating and caching an enormous array
      that represents all of the connections.
The max_depth represents the maximum depth of the array.
For deeply nested entity references you may need to change this value.

## USAGE

To use the Entity Tracer:

- Go to `/admin/config/development/entity-tracer`.
- Select the entity type you want to trace.
- Choose the bundle (dropdown auto-updates based on entity type or direction).
- Select the tracing direction:
  - "Down": Lists all entity reference fields and their nested entities
    for the selected bundle.
  - "Up": Finds instances where other entities reference the selected bundle.

Once the selections are made, they're used to generate a diagram showing all
the requested entity relationships.
This diagram is nested and will go as far as the max_depth value allows.
The fields are bolded and the entities being referenced will link to
said entity's field page.

## MAINTAINERS

- [Ryan Loos](https://www.drupal.org/u/rloos289)
- [Chapter Three](https://www.drupal.org/chapter-three)
  Sponsored the module development and ongoing support
